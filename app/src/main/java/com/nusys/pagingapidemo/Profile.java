package com.nusys.pagingapidemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Profile extends AppCompatActivity {
    RecyclerView post_profile_list;
    AdapterProfilePost myAdapter;
    private List<ProfilePostModel> ruleslist;
    //SharedPreference_main SharedPreference_main;
    TextView btn_edit_profile, user_name, user_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setTitle("Profile");

        hitProfilePostApi();
        // SharedPreference_main = SharedPreference_main.getInstance(this);


        user_name = findViewById(R.id.user_name);
        user_email = findViewById(R.id.user_email);
        // user_name.setText(SharedPreference_main.getUsername());
        //user_email.setText(sharedPreference_main.getUserEmail());

        btn_edit_profile = findViewById(R.id.edit_profile);

        post_profile_list = findViewById(R.id.post_profile_list);
        //LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        post_profile_list.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new AdapterProfilePost(Profile.this, ruleslist);
        post_profile_list.setAdapter(myAdapter);


    }


    private void hitProfilePostApi() {
        Toast.makeText(Profile.this, "sfsfskf", Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id", "12");
            jsonBody.put("page", "1");


        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        // String url = getResources().getString(R.string.url);
        String URL = "http://13.233.162.24/nusys(UAT)/api/student/profile.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            Toast.makeText(Profile.this, "sfsfskf", Toast.LENGTH_SHORT).show();
                            if (status) {
                                final List<ProfilePostModel> ab = new ArrayList<>();
                                JSONObject jsonObj = response.getJSONObject("data");
                                JSONArray jsonArray = jsonObj.getJSONArray("posts");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    ProfilePostModel entity = new ProfilePostModel();
                                    //  entity.setData(response.getString("namezz"));
                                    entity.setUploader_name(jsonObject.getString("uploader_name"));
                                    entity.setHeading(jsonObject.getString("heading"));
                                    entity.setContent(jsonObject.getString("content"));
                                    entity.setFilename(jsonObject.getString("filename"));
                                    entity.setTime(jsonObject.getString("time"));
                                    entity.setLikes(jsonObject.getString("likes"));
                                    entity.setTotal_comments(jsonObject.getString("total_comments"));

                                    ab.add(entity);
                                }
                                //reprog.setVisibility(View.GONE);
                                myAdapter = new AdapterProfilePost(Profile.this, ab);
                                post_profile_list.setAdapter(myAdapter);

                                //Toast.makeText(SignUpStepTwo.this, "true"+response.getString("message"), Toast.LENGTH_SHORT).show();
                                //  Toast.makeText(SignUpStepTwo.this, "true", Toast.LENGTH_SHORT).show();
                            } else {
                                // reprog.setVisibility(View.GONE);
                                //  tv_no_record.setVisibility(View.VISIBLE);

                                // Toast.makeText(SignUpStepTwo.this, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_SHORT).show();
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lIjoxNTc4NDgwNDI1LCJkYXRhIjp7ImlkIjoiMiIsIm5hbWUiOiJwcmF2ZWVuIiwiZW1haWwiOiJwcmF2aW5rdW1hcjE1NTk3QGdtYWlsLmNvbSJ9fQ._bJCyLGrb8OZbr0G1yXmPI_3dCycIiDJxLHKNVxWpHQ");
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }


}

package com.nusys.pagingapidemo;

public interface PaginationAdapterCallback {

    void retryPageLoad();
}

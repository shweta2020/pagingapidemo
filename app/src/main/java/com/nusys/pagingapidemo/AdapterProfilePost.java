package com.nusys.pagingapidemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;

import java.util.List;


public class AdapterProfilePost extends RecyclerView.Adapter<AdapterProfilePost.ItemViewHolder> {

    List<ProfilePostModel> question_list;
    private Context a;

    public AdapterProfilePost(Context context, List<ProfilePostModel> question_list) {
        this.a = context;
        this.question_list = question_list;
    }
    @Override
    public AdapterProfilePost.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_adapter, parent ,false);
        AdapterProfilePost.ItemViewHolder viewHolder = new AdapterProfilePost.ItemViewHolder(v);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(AdapterProfilePost.ItemViewHolder holder, final int position) {
        ProfilePostModel profilePostM = question_list.get(position);
        holder.uploader_name.setText(profilePostM.getUploader_name());
        holder.post_time.setText(profilePostM.getTime());
        holder.tv_heading.setText(profilePostM.getHeading());
        holder.tv_content.setText(profilePostM.getContent());
        holder.tv_likes.setText(profilePostM.getLikes());
        holder.tv_comment.setText(profilePostM.getTotal_comments());

      Glide.with(a).load(profilePostM.getUploader_image()).into(holder.uploader_img);
        Glide.with(a).load(profilePostM.getFilename()).into(holder.feed_image);
        //holder.id.setText(question_list.get(position).getId());

    }

    @Override
    public int getItemCount() {
        //return question_list.size();

        return (null !=question_list ? question_list.size() :0);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView uploader_name,post_time,tv_heading,tv_content,tv_likes, tv_comment;
        ImageView uploader_img,feed_image;
        public ItemViewHolder(View itemView) {
            super(itemView);
            uploader_img = itemView.findViewById(R.id.uploader_img);
            uploader_name = itemView.findViewById(R.id.user_name);
            post_time = itemView.findViewById(R.id.post_time);
            tv_heading = itemView.findViewById(R.id.tv_heading);
            feed_image = itemView.findViewById(R.id.feed_image);
            tv_content = itemView.findViewById(R.id.tv_content);
            tv_likes = itemView.findViewById(R.id.tv_likes);
            tv_comment = itemView.findViewById(R.id.tv_comment);


           // uploader_name = itemView.findViewById(R.id.user_name);

            //id = itemView.findViewById(R.id.name_id);

        }
    }

}